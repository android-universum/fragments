@Fragments-Transition
===============

This module groups the following modules into one **single group**:

- [Transition-Core](https://bitbucket.org/android-universum/fragments/src/main/library-transition-core)
- [Transition-Common](https://bitbucket.org/android-universum/fragments/src/main/library-transition-common)
- [Transition-Extra](https://bitbucket.org/android-universum/fragments/src/main/library-transition-extra)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Afragments/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Afragments/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:fragments-transition:${DESIRED_VERSION}@aar"

_depends on:_
[fragments-core](https://bitbucket.org/android-universum/fragments/src/main/library-core),
[fragments-manage-core](https://bitbucket.org/android-universum/fragments/src/main/library-manage-core)