Fragments-Web
===============

This module contains web related `Fragment` implementations.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Afragments/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Afragments/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:fragments-web:${DESIRED_VERSION}@aar"

_depends on:_
[fragments-core](https://bitbucket.org/android-universum/fragments/src/main/library-core),
[fragments-base](https://bitbucket.org/android-universum/fragments/src/main/library-base),
[fragments-common](https://bitbucket.org/android-universum/fragments/src/main/library-common)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [WebFragment](https://bitbucket.org/android-universum/fragments/src/main/library-web/src/main/java/universum/studios/android/fragment/WebFragment.java)